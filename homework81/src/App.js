import React, { Component } from 'react';

import './App.css';
import Link from "./container/Link/Link";

class App extends Component {
    render() {
        return (
            <div className="App">
                <Link />
            </div>
        );
    }
}

export default App;
