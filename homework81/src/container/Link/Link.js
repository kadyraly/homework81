import React, { Component } from 'react';
import './Link.css';
import {connect} from 'react-redux';


import LinkForm from "../../component/LinkForm/LinkForm";
import {fetchLinks} from "../../store/action/action";

class Link extends Component {




    componentDidMount() {
        this.props.fetchLinks();

    };


    render() {


        return (
            <div className="App">

                <div className="task-wrapper">
                    <LinkForm />
                    <a href={"http://localhost:8000/links/" + this.props.links}>http://localhost:8000/{this.props.links}</a>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        links: state.links
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchLinks: () => dispatch(fetchLinks()),


    }
};


export default connect(mapStateToProps, mapDispatchToProps)(Link);