import React, {Component} from 'react';
import './LinkForm.css';
import {connect} from 'react-redux';
import {createLink} from "../../store/action/action";
class LinkForm extends Component {


    state = {
        originalUrl: ''
    };



    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };
    render() {
        return (
            <div>
                <div>
                    <label>Link:</label>
                    <input
                        type="text"
                        value={this.state.link}
                        placeholder="link here..."
                        className="form-control"
                        name="originalUrl"
                        onChange={this.inputChangeHandler}
                    />
                </div>

                <div>
                    <button  onClick={() => this.props.createLink(this.state)} >Shorten</button>
                </div>
            </div>
        );
    }
};


const mapDispatchToProps = dispatch => {
    return {
        createLink: (Data) => dispatch(createLink(Data))
    }
};

export default connect(null, mapDispatchToProps)(LinkForm);