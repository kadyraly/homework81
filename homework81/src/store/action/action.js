import axios from '../../axios-api';
import {CREATE_LINK_SUCCESS, FETCH_LINKS_SUCCESS} from "./actionTypes";


export const fetchLinksSuccess = links => {
    return {type: FETCH_LINKS_SUCCESS, links};
};

export const fetchLinks = () => {
    return dispatch => {
        axios.get('/links').then(
            response => dispatch(fetchLinksSuccess(response.data))
        );

    };
};

export const createLinkSuccess = (data) => {
    return {type: CREATE_LINK_SUCCESS, data};
};

export const createLink = Data => {
    return dispatch => {
        return axios.post('/links', Data).then(
            response => dispatch(createLinkSuccess(response.data.shortUrl))
        );
    };
};

