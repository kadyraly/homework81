const express = require('express');


const path = require('path');
const Link = require('../model/Link');
const nanoid = require('nanoid');
const  config = require('../config');
const router = express.Router();

const ObjectId = require('mongodb').ObjectId;



const createRouter = (db) => {
    // Product index
    router.get('/', (req, res) => {
        Link.find()
            .then(results => res.send(results))
            .catch(() => res.sendStatus(500));
    });

    // Product create
    router.post('/', (req, res) => {

        const linkData = req.body;
        linkData.shortUrl = nanoid(6);

        const link = new Link(linkData);

        link.save()
            .then(result => res.send(result))
            .catch(() => res.sendStatus(500));

    });

    // Product get by ID
    router.get('/:id', (req, res) => {
        Link
            .findOne({shortUrl: req.params.id}, (err, result) => {
                if (result) {
                    res.status(301).redirect(result.originalUrl)
                } else {res.sendStatus(404)}
            })

    });

    return router;
};

module.exports = createRouter;