const express = require('express');
const cors  = require('cors');

const mongoose = require('mongoose');
const config = require('./config');

const link = require('./app/link');
const app = express();

const port = 8000;



app.use(express.json());
app.use(cors());
app.use(express.static('public'));

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;
db.once('open', db => {
    console.log('Mongoose was loaded!');

app.use('/links', link(db));

app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
});
});