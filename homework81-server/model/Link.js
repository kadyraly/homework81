const mongoose = require('mongoose');
const ProductSchema = new mongoose.Schema({
    shortUrl: {
        type: String,
        required: true
    },
    originalUrl: {
        type: String,
        required: true
    }

});

const Link = mongoose.model('Link', ProductSchema);
module.exports = Link;